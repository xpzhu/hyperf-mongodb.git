<?php

declare(strict_types=1);

namespace Hyperf\Mongodb\Pool;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\ConnectionInterface;
use Hyperf\Mongodb\MongoDbConnection;
use Hyperf\Pool\Pool;
use Hyperf\Utils\Arr;
use Psr\Container\ContainerInterface;

class MongoDBPool extends Pool
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $config;

    /**
     * MongoDBPool constructor.
     * @param ContainerInterface $container
     * @param string             $name
     */
    public function __construct(ContainerInterface $container, string $name)
    {
        $this->name = $name;
        $config     = $container->get(ConfigInterface::class);
        $key        = sprintf('mongodb.%s', $this->name);
        if (!$config->has($key)) {
            throw new \InvalidArgumentException(sprintf('config[%s] is not exist!', $key));
        }

        $this->config = $config->get($key);
        $options      = Arr::get($this->config, 'pool', []);

        parent::__construct($container, $options);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * 创建连接
     * @return ConnectionInterface
     */
    protected function createConnection(): ConnectionInterface
    {
        return new MongoDbConnection($this->container, $this, $this->config);
    }
}